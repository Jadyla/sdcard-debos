#!/bin/bash
if [ ! -f /etc/wpa_supplicant/wpa_supplicant.conf ]; then
     echo "Configuring wifi"
     device=$(ifconfig | grep w | grep : | grep mtu | awk '{printf $1}')
     if [ "$device" != "" ]; then
          echo "Wifi device found, configuring wifi"
          echo "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev" > /etc/wpa_supplicant/wpa_supplicant.conf
          echo "update_config=1" >> /etc/wpa_supplicant/wpa_supplicant.conf
          echo "country=BR" >> /etc/wpa_supplicant/wpa_supplicant.conf
          echo "network={" >> /etc/wpa_supplicant/wpa_supplicant.conf
          echo "    ssid="ssid"" >> /etc/wpa_supplicant/wpa_supplicant.conf
          echo "    psk="password"" >> /etc/wpa_supplicant/wpa_supplicant.conf
          echo "}" >> /etc/wpa_supplicant/wpa_supplicant.conf
          update-initramfs -u
          ip link set $device up
          wpa_supplicant -B -i $device -c /etc/wpa_supplicant/wpa_supplicant.conf -D nl80211
          systemctl restart NetworkManager
          reboot
     else
          echo "No wifi device found, waiting for the next reboot"
     fi
else
     systemctl stop rasp-config
     systemctl disable rasp-config
     echo "Everything is already configured"
fi
echo "Done"