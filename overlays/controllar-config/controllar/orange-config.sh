#!/bin/bash
if [ $(grep -c "dtb" /boot/extlinux/extlinux.conf) -eq "0" ]; then
     echo "Setting dtb"
     kernel_version=$(uname -r)
     fisrt_text=$(cat /boot/extlinux/extlinux.conf | grep "initrd" -B 16 | head -n 17)
     second_text=$(cat /boot/extlinux/extlinux.conf | grep "append root" -A 5 | head -n 6)
     third_text=$(cat /boot/extlinux/extlinux.conf | grep "append root" | tail -n 1)
     echo "$fisrt_text" > /tmp/extlinux.conf
     echo "        #fdtdir /usr/lib/linux-image-$kernel_version/" >> /tmp/extlinux.conf
     echo "        fdt /boot/dtb-$kernel_version/sun8i-h3-orangepi-pc.dtb" >> /tmp/extlinux.conf
     echo "        fdtoverlays /boot/dtb-$kernel_version/overlay/sun8i-h3-uart3.dtbo" >> /tmp/extlinux.conf
     echo "$second_text" >> /tmp/extlinux.conf
     echo "        #fdtdir /usr/lib/linux-image-$kernel_version/" >> /tmp/extlinux.conf
     echo "        fdt /boot/dtb-$kernel_version/sun8i-h3-orangepi-pc.dtb" >> /tmp/extlinux.conf
     echo "        fdtoverlays /boot/dtb-/$kernel_version/overlay/sun8i-h3-uart3.dtbo" >> /tmp/extlinux.conf
     echo "$third_text" >> /tmp/extlinux.conf
     mv /tmp/extlinux.conf /boot/extlinux/extlinux.conf
     reboot
elif [ ! -f /lib/firmware/rtlwifi/rtl8188fufw.bin ]; then
     echo "Installing rtl8188fufw"
     dkms install /controllar/rtl8188fu
     cp /controllar/rtl8188fu/firmware/rtl8188fufw.bin /lib/firmware/rtlwifi/
     reboot
elif [ ! -f /etc/wpa_supplicant/wpa_supplicant.conf ]; then
     echo "Configuring wifi"
     device=$(ifconfig | grep w | grep : | grep mtu | awk '{printf $1}')
     if [ "$device" != "" ]; then
          echo "Wifi device found, configuring wifi"
          echo "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev" > /etc/wpa_supplicant/wpa_supplicant.conf
          echo "update_config=1" >> /etc/wpa_supplicant/wpa_supplicant.conf
          echo "country=BR" >> /etc/wpa_supplicant/wpa_supplicant.conf
          echo "network={" >> /etc/wpa_supplicant/wpa_supplicant.conf
          echo "    ssid="ssid"" >> /etc/wpa_supplicant/wpa_supplicant.conf
          echo "    psk="password"" >> /etc/wpa_supplicant/wpa_supplicant.conf
          echo "}" >> /etc/wpa_supplicant/wpa_supplicant.conf
          update-initramfs -u
          modprobe -r rtl8188fu
          modprobe rtl8188fu
          ip link set $device up
          wpa_supplicant -B -i $device -c /etc/wpa_supplicant/wpa_supplicant.conf -D nl80211
          systemctl restart NetworkManager
          reboot
     else
          echo "No wifi device found, waiting for the next reboot"
     fi
else
     systemctl stop orange-config
     systemctl disable orange-config
     echo "Everything is already configured"
fi
echo "Done"