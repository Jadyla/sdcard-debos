#!/bin/bash
cd /controllar/docker-images/api-server-tasmota
export DOCKER_CONFIG=~/.api-server-tasmota
docker-compose pull api-server-tasmota
docker-compose down
docker-compose up -d
docker image prune -f
