#!/bin/bash
cd /controllar/docker-images/fruit-server
export DOCKER_CONFIG=~/.fruit-server
docker-compose pull fruit-server
docker-compose up -d
docker image prune -f
