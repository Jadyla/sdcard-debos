#!/bin/bash
systemctl stop avahi-daemon
systemctl stop avahi-daemon.socket
cd /controllar/docker-images/tasmota-mqtt
export DOCKER_CONFIG=~/.tasmota-mqtt
docker-compose pull tasmota-mqtt
docker-compose up -d
docker image prune -f
