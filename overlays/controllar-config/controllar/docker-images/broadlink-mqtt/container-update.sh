#!/bin/bash
cd /controllar/docker-images/broadlink-mqtt
export DOCKER_CONFIG=~/.broadlink-mqtt
docker-compose pull broadlink-mqtt
docker-compose down
docker-compose up -d
docker image prune -f
