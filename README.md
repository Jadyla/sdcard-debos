# Debos Generate Image

### 💻 Techs
<div style="display: flex;">
<br>
<img src="https://www.debian.org/Pics/openlogo-50.png" width="50">
<br>
<img src="https://static.vivaolinux.com.br/imagens/tux-linux.jpg" width="50">
<br>
</div>

### 👋 Hey! :)
This code has been development to generate bootable debian images for single-board computers in an automated way. This image needs to have specified packages and files to be use in a automated project. The idea is to control the packages and files necessary before the image be generate, so that, after the image is generated and bootable on <i>SBC</i>, it doesn't need more maintenance or alter on data. And to be possible that, was used the DEBOS project, that allows to control the final image and the scripts executed on system startup.

You can see the DEBOS documentation <a href="https://github.com/go-debos/debos">here</a>.

### 📚 YAML FILE

All informations that will be on final generated image, is setup in a <b>YAML</b> file, this
file has all necessary specifications for project.
```yaml
architecture: armhf

actions:

- action: apt
description: Install orangepi packages
packages:
- gcc-arm-linux-gnueabihf
- python3-dev
- rustc
- cargo
- python3-cryptography
- sudo
- adduser
- systemd-sysv
- initramfs-tools
- u-boot-tools
- u-boot-menu
- util-linux
```

### 💾 The project

The automation project consists in a SBC with a system that controlls diferents places in a house. This single-board computer can be a Raspberry Pi or an Orange Pi and needs has controll files and scripts to use at home.

For don't manipule this scripts on Board, was used the DEBOS project for control all image generated process and store files.

The execution sequence on create image proccess by DEBOS it's made for yaml files in project root. There is three YAML files:

<ul>
<li>controllar-debimage-orangepi.yaml</li>
<li>controllar-debimage-raspberrypi.yaml</li>
<li>controllar-generate-image.yaml</li>
</ul>

# <h3>controllar-generate-image.yaml:</h3>

There is common configs in two images generate process. For example:
<ul>
<li>Download broadlink-mqtt repo</li>
<li>Download codes</li>
<li>Install commmon packages</li>
<li>Download Debian Repository to packages instalations</li>

<li>Files config into PCB</li>
<li>Exec Scripts from project</li>
</ul>

# <h3>controllar-debimage-raspberrypi.yaml:</h3>

There is configs for raspberry build image process:

<ul>
<li>Download Firmware of board</li>
<li>Instalation of Firmware</li>
<li>Instalation of Kernel</li>
<li>Mount and compact image with especific patitions for boot execution</li>
</ul>

# <h3>controllar-debimage-orangepi.yaml:</h3>

There is configs for orange build image process:
<ul>
<li>Download od necessary and specific packages from armhf architecture (the broadlink-mqtt for armhf needs some packages and the rasp don't. like <u> gcc-arm-linux-gnueabihf</u>,<u> python3-dev</u>,<u> rustc</u>,<u> cargo</u>,<u> cryptography</u>)</li>
<li>Mount and compact image with especific partitions for boot execution</li>
<li>Instalation of U-BOOT, this tool is necessary for system start on board (
Here needs some refactors because the U-BOOT config with error, is preventing the kernel modify the serial states)</li>
</ul>

### 🎬 To execute

To exec DEBOS, just exec the command:

```
debos file.yaml
```

However, some params <b>must</b> be passed for YAML file script be executed. These parameters are: 
<ul>
<li><i>docker_registry_api_server_passwd</i>: DOCKER api-server container password</li>
<li><i>docker_registry_api_server_controllar_ml_passwd</i>: DOCKER api-server-controllar-ml container password</li>
<li><i>docker_registry_api_server_tasmota_passwd</i>: DOCKER api-server-tasmota container password</li>
<li><i>docker_registry_broadlink_mqtt_passwd</i>: DOCKER broadlink-mqtt container password</li>
<li><i>docker_registry_tasmota_mqtt_passwd</i>: DOCKER broadlink-mqtt container password</li>
<li><i>docker_registry_fruit_server_passwd</i>: DOCKER fruit-server container password</li>
<li><i>board</i>: board type for verify the architecture and commands</li>
<li><i>root_passwd</i>: board password</li>
<li><i>date</i>: date that image was generate, thats very important to thecnic see the version</li>
</ul>

You can also add other parameters, <b>not mandatory</b>*:

<ul>
<li>
  <i>docker_version</i>: select the services that will be enable on image, to default, the paramter is <b>"" (null)</b> and means that just the simple services will be enable ("Gestão" and "Mapinha")
    <ul>
      <li><b>"multimidia-tasmota-ml"</b>: For images with multimedia, tasmota and ml services</li>
      <li><b>"multimidia-tasmota"</b>: For images with multimedia and tasmota services</li>
      <li><b>"multimidia-ml"</b>: For images with multimedia and ml services</li>
      <li><b>"tasmota-ml"</b>: For images with tasmota and ml services</li>
      <li><b>"multimidia"</b>: For images just with multimedia service</li>
      <li><b>"tasmota"</b>: For images just with tasmota service</li>
      <li><b>"ml"</b>: For images just with ml service</li>
      <li><b>""</b>: For images without new services, just with simple services (<i>default</i>)</li>
    </ul>
</li>
<li><i>make_hex_version</i>: MakeHex project that is add to /controllar/MakeHex</li>
<li><i>irdb_version</i>: irdb project that is add to /controllar/irdb</li>
</ul>

*Change the arguments that are not mandatory is useful just for tests. You can generate an image only using docker registrys passwords, board and root_passwd.


In addition, The fake-machine (U-BOOT part on project) have a error on execution, the it needs disable with the flag `--disable-fakemachine`.

The complete command will be this way:

```
debos -m 8G -t docker_registry_api_server_passwd:pass -t docker_registry_api_server_controllar_ml_passwd:pass -t docker_registry_broadlink_mqtt_passwd:pass -t docker_registry_tasmota_mqtt_passwd:pass -t docker_registry_fruit_server_passwd:pass -t docker_registry_api_server_tasmota_passwd:pass -t board:raspberrypi -t root_passwd:senharoot -t docker_version:"multimidia-tasmota-ml" -t date:"12-jun-2024" controllar-generate-image.yaml --disable-fakemachine
```

The -m flag its related with the DEBOS memory.

### 💿 Set up the image with maleta.sh

After generate the image is necessary add the .db3 and .ovpn files. You can add those using the interface at <a href="https://bit.ly/controllar_sdcard">https://bit.ly/controllar_sdcard</a> or follow the step guide below.

You must unpack the generated image to another folder, since you will edit the image.

#### Create <i>mount</i> folder

The first thing to do is create one folder named <b>mount</b> in the same directory of image copy. This folder will be present in all the process of generate a complete image. 

After create this folder you can execute the follow command to see how many bytes are the sectors and also where start the partition (in Raspberry Pi, the second partition). You must multiply those values (usually 128000512 for Raspberry Pi):

```
fdisk -l <imageName>.img
```

Now you need to generate the archives for <i>mount</i> folder using the commands: 

```
sudo mount -t ext4 -o rw,sync,offset=<valueFromPreviousStep> <ImageName>.img mount
```

####  Add .db3 and .ovpn

Copy the .db3 and .ovpn files from your computer to the board:

```
sudo cp <db3FileDirectory>.db3 mount/controllar
sudo cp <ovpnFileDirectory>.ovpn mount/controllar
```

####  Execute <i>maleta.sh</i>

Access the image with:

```
sudo chroot mount /bin/bash
```

Get into controllar folder and execute <i>maleta.sh</i>

```
cd /controllar
./maleta.sh
```

Choose the option. The current configuration is 1 - 3 - 1 for Raspberry Pi and 1 - 1 - 1 for Orange Pi.


When you're done, check the <i>config.ini</i> to see if the files are correct.

### 🛜 Wi-Fi for your board

In the controllar folder is a file named <i>setup-wifi.sh</i>. To enable Wi-Fi on your board you can just run this file passing as parameter the <b>ssid</b> and <b>password</b>:

```
./setup-wifi.sh "SSID" "PASSWORD"
```

If you are using Orange Pi it's necessary that you have an Adaptador Wireless Usb. When you run the code the board will be rebooted to save driver informations for the adaptador. For Raspberry Pi it's not necessary.